# coding: utf-8

Gem::Specification.new do |spec|
  spec.name          = "wkhtmltopdf-binary"
  spec.version       = "0.11.0.rc1"
  spec.authors       = ["Zakir Durumeric"]
  spec.email         = ["zakird@gmail.com"]
  spec.summary       = %q{Provides binaries for WKHTMLTOPDF project in an easily accessible package.}
  spec.has_rdoc      = false

  spec.files         = `find bin/*`.split $/ 
  spec.executables   = ['wkhtmltopdf']
  spec.require_paths = ["."]

  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"
end
